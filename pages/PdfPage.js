import Head from "next/head";
import React from "react";
import NavBar from "../components/NavBar";
import { Page, Text, Document, StyleSheet, PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';

// Create styles
const styles = StyleSheet.create({
    body: {
        paddingTop: 35,
        paddingBottom: 65,
        paddingHorizontal: 35,
    },
    title: {
        fontSize: 24,
        textAlign: "center",
    },
    text: {
        margin: 12,
        fontSize: 14,
        textAlign: "justify",
        fontFamily: "Times-Roman",
    },
    textItalic: {
        margin: 12,
        fontSize: 14,
        textAlign: "justify",
        fontFamily: "Times-Italic",
        textAlign: "center"
    },
    pageNumber: {
        position: "absolute",
        fontSize: 12,
        bottom: 30,
        left: 0,
        right: 0,
        textAlign: "center",
        color: "grey",
    }
});

// Create Document Component
const MyDocument = () => (
    <Document className="">
        <Page style={styles.body}>
            <Text style={styles.title}>
                Cara Bermain
            </Text>
            <Text style={styles.text}>
                1. Login atau daftar akun terlebih dahulu
            </Text>
            <Text style={styles.text}>
                2. Masuk ke menu Game
            </Text>
            <Text style={styles.text}>
                3. Terdapat 3 Pilihan Gunting, Batu, & Kertas, pilih salah satu
            </Text>
            <Text style={styles.text}>
                4. Komputer akan memilih secara acak lalu hasil permainan akan muncul
            </Text>
            <Text style={styles.textItalic}>
                Selamat Bertanding, junjung tinggi sportifitas!
            </Text>
            <Text
                style={styles.pageNumber}
                render={({ pageNumber, totalPages }) =>
                    `${pageNumber} / ${totalPages}`
                }
            />
        </Page>
    </Document>
);

export default function App() {
    return (
        <>
            <Head>
                <title>GameId | LeaderBoards</title>
            </Head>
            <NavBar />
            <div className="text-center">
                <PDFViewer className="container-full bg-[#252525] w-full lg:h-[52rem] flex justify-between">
                    <MyDocument />
                </PDFViewer>
            </div>
            <PDFDownloadLink document={<MyDocument />} fileName="LeaderBoard" className="pb-8 flex justify-center">
                <button className="mt-12 bg-[#F1B03D] text-[#161616] font-medium text-lg rounded-[18px] px-[30px] py-[10px]">
                    Download
                </button>
            </PDFDownloadLink>
        </>
    );
}