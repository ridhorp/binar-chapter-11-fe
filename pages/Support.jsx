import Link from "next/link";
import React from "react";
import { ControlBar, Player } from "video-react";
import "video-react/dist/video-react.css";
import { Upload } from "../components/Upload";

function Support() {
  return (
    <div>
      <div className="text-black m-5 cursor-pointer text-base lg:text-lg flex items-center hover:border-b-[3px] border-[#2B9EDE] absolute">
        <img src="../👈.svg" alt="" className="mr-1 md:mr-2 h-4 lg:h-5" />
        <Link href={"/"}>Back to homepage</Link>
      </div>
      <div className="container w-6/12 mx-auto mt-20">
        <Player autoPlay>
          <source src={"video1.mp4"}></source>
          <ControlBar autoHide={true} />
        </Player>
      </div>
      <Upload />;
    </div>
  );
}

export default Support;
