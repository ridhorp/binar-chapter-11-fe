import { Document, Page, StyleSheet, Text } from "@react-pdf/renderer";
import React from "react";

// Create styles
const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  title: {
    fontSize: 24,
    textAlign: "center",
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "Times-Roman",
  },
  textItalic: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "Times-Italic",
    textAlign: "center",
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
  },
});

function MyDocument() {
  return (
    <Document className="">
      <Page style={styles.body}>
        <Text style={styles.title}>Cara Bermain</Text>
        <Text style={styles.text}>
          1. Login atau daftar akun terlebih dahulu
        </Text>
        <Text style={styles.text}>2. Masuk ke menu Game</Text>
        <Text style={styles.text}>
          3. Terdapat 3 Pilihan Gunting, Batu, & Kertas, pilih salah satu
        </Text>
        <Text style={styles.text}>
          4. Komputer akan memilih secara acak lalu hasil permainan akan muncul
        </Text>
        <Text style={styles.textItalic}>
          Selamat Bertanding, junjung tinggi sportifitas!
        </Text>
        <Text
          style={styles.pageNumber}
          render={({ pageNumber, totalPages }) =>
            `${pageNumber} / ${totalPages}`
          }
        />
      </Page>
    </Document>
  );
}

export default MyDocument;
