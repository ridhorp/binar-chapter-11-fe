import { useState } from "react";
import { Spinner } from "../components/Spinner";

export function Upload() {
  const [showSpinner, setShowSpinner] = useState(false);
  const onChange = async (event) => {
    setShowSpinner(true);
    event.preventDefault();
    const formData = new FormData();
    const file = event.target.files[0];
    formData.append("inputFile", file);

    try {
      const response = await fetch("/api/upload", {
        method: "POST",
        body: formData,
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      setShowSpinner(false);
    } finally {
      setShowSpinner(false);
    }
  };

  return (
    <>
      <input type="file" onChange={onChange} />
      <Spinner displayed={showSpinner} />
    </>
  );
}
