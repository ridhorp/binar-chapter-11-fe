import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import ButtonPrimary from "../components/ButtonPrimary";
import SignIn from "../pages/SignIn";

// Positive
describe("Success Render Button", () => {
  it("renders a primary button", () => {
    render(<ButtonPrimary />);

    expect(screen.getByTestId("buttonprimary")).toBeInTheDocument();
  });
});

describe("Home", () => {
  it("renders a card game", () => {
    render(<SignIn />);

    const heading = screen.getByRole("heading", {
      name: /welcome to next\.js!/i,
    });

    expect(heading).toBeInTheDocument();
  });
});
